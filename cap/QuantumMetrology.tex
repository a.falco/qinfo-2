\cleardoublepage
\section{Quantum metrology}

La quantum metrology è lo studio del recupero di parametri codificati nei messaggi. Per inquadrare il problema partiamo dalla \emph{classical estimation theory}.

\subsection{Classical estimation theory}
Sia $\lambda\in I$ un parametro che vogliamo determinare, noi abbiamo solamente accesso a una variabile stocastica $X$, connessa a $\lambda$ da una probabilità condizionale $p(x\vert\lambda)$. Per farci un'idea possiamo immaginare che $x$ sia il risultato di una misura fatta sul sistema\footnote{Ad esempio la variabile stocastica $X$ è rappresentata da una distribuzione Gaussiana la cui larghezza dipende in qualche modo da $\lambda$. Quello che vogliamo fare è quindi ricavare $\lambda$ dalle realizzazioni (misure) di $X(\lambda)$.}. Supponiamo di fare tante misure e immaginiamo che queste siano connesse dalla stessa probabilità condizionale:
\begin{equation}
	\vb{x}=(x_1,\ldots,x_n)^t \,,
	\qquad\qquad
	p(\vb{x}\vert\lambda)=\prod_{j=1}^n p(x_j\vert\lambda) \,.
\end{equation}
Poi faremo un data processing che ci darà una stima $\lambda_\text{est}$ di $\lambda$. Quello che vogliamo fare è, dato $p(\vb{x}\vert\lambda)$, ottimizzare $\lambda_\text{est}$. Introduciamo la seguente figura di merito
\begin{definition}[RMSE]
	Data una variabile stocastica $\vb{X}(\lambda)$ dipendente dal parametro $\lambda$ e una stima $\lambda_\text{est}$ di quest'ultimo, si definisce il \emph{root mean square error} (RMSE) come
	\begin{equation}
		\Delta\lambda=\sqrt{\int\dd{\vb{x}}p(\vb{x}|\lambda)\abs{\lambda-\lambda_\text{est}}} \,.
	\end{equation}
	%Root mean square error (RMSE).
\end{definition}

\begin{comment}
Siamo interessati ai processi di stima che restano vicini a $\lambda$ al crescere di $n$.
\begin{equation}
	\lambda_\text{est}\xrightarrow[n\to+\infty]{}\lambda
\end{equation}
per questo $\lambda_\text{est}$ si dice essere \emph{asymptotically unbiased}.
\end{comment}
\begin{comment}
Siamo interessati ai processi di stima per cui $\lambda_\text{est}$ sia \emph{asymptotically unbiased}, ovvero tale per cui
\begin{equation}
	\lim_{n\to\infty}\lambda_\text{est} = \lambda
\end{equation}
\end{comment}
Siamo interessati ai processi di stima che restino vicini a $\lambda$ al crescere di $n$. In particolare a quei processi tali per cui
\begin{equation}
	\lim_{n\to\infty}\lambda_\text{est} = \lambda \,.
\end{equation}
Una stima $\lambda_\text{est}$ che soddisfa questa proprietà si dice essere \emph{asymptotically unbiased}.

\begin{theorem}[Cramer-Rao bound]
	Vale quello che prende il nome di \emph{Cramer-Rao} bound, i.e.,
	\begin{equation}
		\boxed{
			\Delta\lambda\geq\frac{1}{\sqrt{n\,F(\lambda)}} \,,
		}
	\end{equation}
	dove
	\begin{equation}
		F(\lambda)=\int\dd{\vb{x}}\frac{1}{p(\vb{x}\vert\lambda)}\abs{\pdv{p(\vb{x}\vert\lambda)}{\lambda}} \,.
	\end{equation}
	Inoltre questa disuguaglianza è saturabile (asintoticamente).
\end{theorem}



\subsection{Quantum estimation theory}
Nel caso quantitico $\lambda$ è un parametro che indicizza gli stati di un sistema $S$. Siamo quindi passati da una descrizione formale a una descrizione fisica. A questo punto abbiamo
\begin{equation}
	p(\vb{x}\vert\lambda)=\tr[\op{E}_{\vb{x}}\op\rho(\lambda)] \,.
\end{equation}
Anche in questo caso faremo la stessa POVM. Adesso rifacciamo data processing classico.

\begin{figure}[H]
	\begin{center}
		\begin{tikzpicture}[scale=1,every text node part/.style={align=center}]
		\def\r{1};
		\def\x{3};
		\def\z{0.6};
		%\draw[gray, very thick] (-5,-3) rectangle (5,3);
		
		\draw (0,0) to[out=+60,in=180] (\x,+2*\z);
		\draw (0,0) to[out=+30,in=180] (\x,+\z);
		\draw (0,0) to[out=0,in=180] (\x,0);
		\draw (0,0) to[out=-30,in=180] (\x,-\z);
		\draw (0,0) to[out=-60,in=180] (\x,-2*\z);
		
		\filldraw[white, draw=black, thick] (0,0) ellipse [x radius =\r, y radius = \r];
		\node [at = {(0,0)}] {$\op\rho(\lambda)$};
		
		\node [at = {(\x,+2*\z)}, anchor=west] {$x_1$};
		\node [at = {(\x,+\z)}, anchor=west] {$x_2$};
		\node [at = {(\x,0)}, anchor=west] {$x_3$};
		\node [at = {(\x,-\z)}, anchor=west] {$x_4$};
		\node [at = {(\x,-2*\z)}, anchor=west] {$x_5$};
		
		\node [at = {(\x+1,0)}, anchor=west] {$\coloneqq\vb{x}\xrightarrow{\text{data processing}}\lambda_\text{est}$};
		\end{tikzpicture}
	\end{center}
\end{figure}


$p(\vb{x}\vert\lambda)=\prod_{j=1}^n p(x_j\vert\lambda)$ Come prima:
\begin{equation}
	\Delta\lambda^{(\text{POVM})} \geq \frac{1}{\sqrt{n\,F_\text{POVM}(\lambda)}} \,.
\end{equation}
Adesso però il lower bound sulla RMSE dipende dalla POVM scelta. Possiamo quindi ottimizzarla e prendere il minimo: 
\begin{theorem}[quantum Cramer-Rao bound]
	\begin{equation}\label{eq:CR-bound}
		\boxed{
			\Delta\lambda^{(\text{POVM})} \geq \frac{1}{\sqrt{n\,QFI(\lambda)}} \,,
		}
	\end{equation}
	dove $QFI$ è la \emph{quantum Fisher information}, definita come
	\begin{equation}
		QFI(\lambda) = \max_\text{POVM} F_\text{POVM}(\lambda) \,.
	\end{equation}
	\footnote{Quantum fisher information per fissato $\lambda\to\op\rho(\lambda)$.}
\end{theorem}


%\orange{
	L'unica cosa su cui non abbiamo ottimizzato è il processo $\lambda\mapsto\op\rho(\lambda)$. Per $\lambda$ che varia nell'intervallo $I$, avremo che il sistema si muoverà lungo una traiettoria (nota). Con la misura vogliamo capire dove, nella traiettoria, si trova lo stato.
%}
\begin{figure}[H]
	\begin{center}
		\begin{tikzpicture}[scale=0.8]
		%raggi
		\def\RR{5}; \def\rr{1.5};
		%arco
		\def\aa{80};
		
		\draw[thick] (\RR*cos{(90-\aa/2)},\RR*sin{(90-\aa/2)}) arc (90-\aa/2:90+\aa/2:\RR);
		%\draw[thick] (\rr*cos{(90-\aa/2)},\rr*sin{(90-\aa/2)}) arc (90-\aa/2:90+\aa/2:\rr);
		\draw[thick] (\rr*cos{(90-\aa/2)},\rr*sin{(90-\aa/2)}) -- (\rr*cos{(90+\aa/2)},\rr*sin{(90+\aa/2)});
		
		\draw[thick] (\RR*cos{(90-\aa/2)},\RR*sin{(90-\aa/2)}) -- (\rr*cos{(90-\aa/2)},\rr*sin{(90-\aa/2)});
		\draw[thick] (\RR*cos{(90+\aa/2)},\RR*sin{(90+\aa/2)}) -- (\rr*cos{(90+\aa/2)},\rr*sin{(90+\aa/2)});
		
		\filldraw[black] (-2,4) circle (2pt);
		\filldraw[black] (2,2.8) circle (2pt);
		\filldraw[black] (-0.2,3.5) circle (0pt) node[anchor=south west, rotate=-20]{$\op\rho(\lambda)$};
		
		\draw[thick,->] (-2,4) to[out=0,in=180] (2,2.8);
		\end{tikzpicture}
	\end{center}
	\caption{Traiettoria di uno stato quantistico.}
\end{figure}
Un esempio può essere $\lambda=1/\beta$ %$\lambda=\frac1\beta$ 
e %$\op\rho=\frac{e^{-\beta\op\H}}{\tr[e^{-\beta\op\H}]}$
$\op\rho=\flatfrac{e^{-\beta\op{H}}}{\tr\qty\big[e^{-\beta\op{H}}]}$. Vogliamo caratterizzare $QFI(\lambda)$. Per farlo, ricordiamo cos'è la fidelity.
\begin{definition}[fidelity]
	Si definisce \emph{fidelity} la quantità
	\begin{equation}
		F(\op\rho,\op\sigma) = \norm{\sqrt{\op\rho}\sqrt{\op\sigma}}_1^2 = \tr\qty[\sqrt{\sqrt{\op\sigma}\,\op\rho\,\sqrt{\op\sigma}}]^2 \,.
	\end{equation}
\end{definition}
\begin{osservazione}
	Possiamo osservare che la fidelity è una specie di inverso della distanza. Infatti, ricordando la definizione di trace distance, $D(\op\rho,\op\sigma)=\norm{\op\rho-\op\sigma}_1$, è facile verificare che:
	\begin{equation}
		%D(\op\rho,\op\sigma)=\norm{\op\rho-\op\sigma}_1
		%\qquad\qquad
		F(\op\rho,\op\sigma)\to1\iff D(\op\rho,\op\sigma)\to0 \,.
	\end{equation}
\end{osservazione}
\begin{definition}[distanza di Brunes]
	%Possiamo anche definire la \emph{distanza di Brunes}:
	Possiamo definire la \emph{distanza di Brunes} come:
	\begin{equation}
		D_B(\op\rho,\op\sigma)=\sqrt{2}\abs{1-\sqrt{F(\op\rho,\op\sigma)}}^{1/2} \,.
	\end{equation}
\end{definition}
\begin{proposizione}
	È possibile dimostrare che
	\begin{equation}
		\boxed{
			QFI(\lambda)=4\lim_{\delta\to0}\frac{D_B^2\qty\big(\op\rho(\lambda+\delta),\op\rho(\lambda))}{\delta^2} \,.
		}
	\end{equation}
\end{proposizione}
Quindi è una sorta di derivata seconda. Si può anche dimostrare che il bound è saturabile (tramite una misura adattiva (locale)).

\subsubsection{Heisenberg scaling}
Inoltre si può dimostrare che il bound \eqref{eq:CR-bound} può essere superato se si ammette la possibilità di fare misure congiunte.
\begin{equation}
	\Delta\lambda\geq\frac1n
\end{equation}
quindi l'errore scala più velocemente. Questo comportamento prende il nome di \emph{Heisenberg scaling}.

\begin{esempio}
	\begin{equation}
		\op\rho(\lambda) = e^{-i\op{H}\lambda}\op\rho(0)e^{i\op{H}\lambda}
	\end{equation}
	unitary encoding ($\lambda$ è una sorta di $t$). Condideriamo lo stato puro $\op\rho(0)=\ketbra{\psi(0)}$. In questo caso abbiamo:\ifdraft\red{lo diamo per buono}\fi
	\begin{equation}
		QFI(\lambda) = 4\ev{\Delta\op{H}^2} = 4\qty(\ev{\op{H}^2}{\psi(0)} - \abs{\ev{\op{H}^2}{\psi(0)}}^2)
	\end{equation}
	\begin{equation}
		\Delta\lambda\geq\frac{1}{2\sqrt{n\ev*{\Delta\op{H}^2}}} \xrightarrow{n=1} \Delta\lambda = \frac{1}{2\sqrt{\ev*{\Delta\op{H}^2}}} = \frac{1}{2\Delta\op{H}}
	\end{equation}
	Infine otteniamo quindi
	\begin{equation}
		\Delta\lambda\,\Delta\op{H} \geq \frac12
	\end{equation}
	dove qui però $\lambda$ non è più un tempo e si ha il principio di indeterminazione. Il tempo non è un'osservabile quindi non potrei definire indeterminazione in senso standard. In teoria dell'informazione però sì.
	
	Fino ad ora abbiamo immaginato $\lambda\to\op\rho(\lambda)$ fissato. In realtà possiamo generalizzare: partendo da uno stato $\op\rho(0)$, c'è un encoding dipendente da $\lambda$. Adesso possiamo anche scegliere lo stato di input $\op\rho(0)$.
	
	%\red{DUE DISEGNI}
	
	Ma adesso, facendolo $n$ volte, possiamo scegliere di mandare stati correlati. Quindi in quantum metrology vogliamo massimizzare questa cosa.
\end{esempio}