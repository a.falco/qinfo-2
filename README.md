# Appunti di IMQT

[![build status](https://gitlab.com/a.falco/appunti-IMQT/badges/master/pipeline.svg)](https://gitlab.com/a.falco/appunti-IMQT/-/jobs/artifacts/master/raw/IMQT.pdf?job=build)

Appunti di Information Methods for Quantum Technologies del corso tenuto alla Scuola Normale Superiore dal Prof. Vittorio Giovannetti durante l'a.a. 2020-2021. L'ultimo pdf è scaricabile [qui](https://gitlab.com/a.falco/appunti-IMQT/-/jobs/artifacts/master/raw/IMQT.pdf?job=build).
